#### MSA_IGNORE_PROC #### # automator launches this entire thing as an argument (bash -c) as opposed to a temp file or STDIN
VER=0.3 # 1.0 does not signify stable (nor does 0.3)!
USER=`whoami`
ENVDUMP=`printenv`
OSXVER=`sw_vers -productVersion`
OUTPATH="~/Desktop/MSA_Log.txt" # tbi
OPTLOCATION=`cat ~/Desktop/msa.txt`
function setopts() {
  IFS=$(echo -en "\n\b") # fix fixes with spaces
  PROCWHITELIST=1
  NEWFILES=1
  OPT_GOOD=0
  for opt in $OPTLOCATION; do
    case $opt in
      "msa:init")
      OPT_GOOD=1
      ;;
      "whitelist:false")
      PROCWHITELIST=0
      ;;
      "whitelist:true")
      PROCWHITELIST=1
      ;;
      "newfiles:false")
      NEWFILES=0
      ;;
      "newfiles:true")
      NEWFILES=1
      ;;
      *)
      OPT_GOOD=0
      ;;
    esac
  done
  if [ "$OPT_GOOD" == "0" ]; then
    echo "INVALID OPTIONS FILE"
    end
  fi
}
function timestamp() {
  date +"%s"
}
function preflight() {
  if [ -a /tmp/msa.lock ]; then
    echo "ERROR: MSA IS ALREADY RUNNING!" >> ~/Desktop/MSA_LOG.txt
    end
  else
    touch /tmp/msa.lock
  fi
}
function enumproc() {
  ps auxe | awk -v var="$(timestamp): " '{print var $0}' >> /tmp/proclist.txt
  IFS=$(echo -en "\n\b") # fix files with spaces
  echo "$(timestamp): WHITELIST: $PROCWHITELIST" >> ~/Desktop/MSA_LOG.txt
  for proc in `cat /tmp/proclist.txt`; do
    if [ "$PROCWHITELIST" == "1" ]; then
      if [ $proc != *"MSA_IGNORE_PROC"* ] || [ $proc != *"CommerceKit.framework"* ] || [ $proc != *"[rcuos/"* ] || [ $proc != *"/sbin/init"* ] || [ $proc != *"[rcuob/"* ] || [ $proc != *"watchdog/"* ] || [ $proc != *"[ksmd]"* ] || [ $proc != *"[hv_vmbus_ctl]"* ] || [ $proc != *"[scsi_eh"* ] || [ $proc != *"ext4-rsv-conver"* ] || [ $proc != *"/sbin/getty -8 38400 tty"* ] || [ $proc != *"/sbin/getty -L ttyS0 115200 vt102"* ] || [ $proc != *"ps aux"* ]; then
        echo "$proc" >> ~/Desktop/MSA_LOG.txt
      fi
    else
      echo "$proc" >> ~/Desktop/MSA_LOG.txt
    fi
  done
}
function enumapps() {
  ls -d1 /Applications/* > /tmp/app.txt
  ls -d1 ~/Applications/* >> /tmp/app.txt
  IFS=$(echo -en "\n\b") # fix files with spaces
  for app in `cat /tmp/app.txt`; do
    if [ ${app: -4} == ".app" ]; then
      /usr/bin/codesign -v "$app"
      if [! $? == "0"]; then
        echo "$(timestamp): [unsigned] $app" >> ~/Desktop/MSA_LOG.txt
      else
        echo "$(timestamp): [signed] $app" >> ~/Desktop/MSA_LOG.txt
      fi
    fi
  done
}
function newfiles() {
  if [[ "$NEWFILES" == "1" ]]; then
    echo "[NEWLY CREATED FILES]" >> ~/Desktop/MSA_LOG.txt
    find /System/Library -mtime -30 | awk -v var="$(timestamp): " '{print var $0}' >> ~/Desktop/MSA_LOG.txt
    echo "[END NEWLY CREATED FILES]" >> ~/Desktop/MSA_LOG.txt
  fi
}
function intcheck() {
  # eventually you will be able to specify SHA1 hashes to manually intcheck; list is not complete!
  skhash=$(shasum /Library/Keychains/System.keychain)
  hohash=$(shasum /etc/hosts)
  xhash=$(shasum /System/Library/CoreServices/CoreTypes.bundle/Contents/Resources/XProtect.plist)
  hostslinecount=$(wc -l < "/etc/hosts")
  echo "$(timestamp): System Roots Keychain: $skhash" >> ~/Desktop/MSA_LOG.txt # detect root modifications
  echo "$(timestamp): Hosts: $hohash" >> ~/Desktop/MSA_LOG.txt # detect hosts modifications
  echo "$(timestamp): XProtect: $xhash" >> ~/Desktop/MSA_LOG.txt # detect malware sig modifications
}
function launchd() {
  launchctl list | awk -v var="$(timestamp): " '{print var $0}' >> ~/Desktop/MSA_LOG.txt
  crontab -l | awk -v var="$(timestamp): " '{print var $0}' >> ~/Desktop/MSA_LOG.txt
}
function netconfig() {
  scutil --dns | awk -v var="$(timestamp): " '{print var $0}' >> ~/Desktop/MSA_LOG.txt
  scutil --proxy | awk -v var="$(timestamp): " '{print var $0}' >> ~/Desktop/MSA_LOG.txt
  scutil --nc list | awk -v var="$(timestamp): " '{print var $0}' >> ~/Desktop/MSA_LOG.txt
  cat /etc/hosts | awk -v var="$(timestamp): " '{print var $0}' >> ~/Desktop/MSA_LOG.txt
}
function kernel() {
  ls -d1 /System/Library/Extensions/* > /tmp/kext.txt
  ls -d1 /Library/Extensions/* >> /tmp/kext.txt
  IFS=$(echo -en "\n\b") # fix files with spaces
  for kext in `cat /tmp/kext.txt`; do
    if [ ${kext: -5} == ".kext" ]; then
      /usr/bin/codesign -v $kext
      if [! $? == "0"]; then
        echo "$(timestamp): [unsigned] $kext" >> ~/Desktop/MSA_LOG.txt
      else
        echo "$(timestamp): [signed] $kext" >> ~/Desktop/MSA_LOG.txt
      fi
    fi
  done
}
function swupdate() {
  defaults read /Library/Preferences/com.apple.SoftwareUpdate | grep CatalogURL | awk -v var="$(timestamp): " '{print var $0}' >> ~/Desktop/MSA_LOG.txt
  softwareupdate -l | awk -v var="$(timestamp): " '{print var $0}' >> ~/Desktop/MSA_LOG.txt
}
function browserext() {
  ls -d1 ~/Library/Application\ Support/Google/Chrome/External\ Extensions/* > /tmp/ext.txt
  ls -d1 /Library/Application\ Support/Google/Chrome/External\ Extensions/* >> /tmp/ext.txt
  IFS=$(echo -en "\n\b") # fix files with spaces
  for ext in `cat /tmp/ext.txt`; do
    if [ ${ext: -5} == ".json" ]; then
      echo "$(timestamp): [external] $ext" >> ~/Desktop/MSA_LOG.txt
    fi
  done
  rm /tmp/ext.txt
  ls -1 ~/Library/Application\ Support/Google/Chrome/Default/Extensions/* > /tmp/ext.txt
  IFS=$(echo -en "\n\b") # fix files with spaces
  for ext in `cat /tmp/ext.txt`; do
    echo "$(timestamp): [cws] $ext" >> ~/Desktop/MSA_LOG.txt
  done
}
function end() {
  echo "[END MSA LOG AT $(timestamp)]" >> ~/Desktop/MSA_LOG.txt
  rm /tmp/msa.lock
  open ~/Desktop/MSA_LOG.txt -a TextEdit
  exit 0
}
rm ~/Desktop/MSA_LOG.txt
touch ~/Desktop/MSA_LOG.txt
if [ -f ~/Desktop/msa.txt ]; then
setopts
fi
echo "[INITIALIZED]" >> ~/Desktop/MSA_LOG.txt
preflight
echo "[BEGIN MSA LOG AT $(timestamp)]" >> ~/Desktop/MSA_LOG.txt
echo "[START LOG INFORMATION]" >> ~/Desktop/MSA_LOG.txt
echo "USER: $USER" >> ~/Desktop/MSA_LOG.txt
echo "MSA VERSION: $VER" >> ~/Desktop/MSA_LOG.txt
echo "OS X VERSION: $OSXVER" >> ~/Desktop/MSA_LOG.txt
echo "BASH VERSION: $BASH_VERSION" >> ~/Desktop/MSA_LOG.txt
echo "[END LOG INFORMATION]" >> ~/Desktop/MSA_LOG.txt
echo "[APPS]" >> ~/Desktop/MSA_LOG.txt
enumapps
echo "[END APPS]" >> ~/Desktop/MSA_LOG.txt
echo "[PROCESSES]" >> ~/Desktop/MSA_LOG.txt
enumproc
echo "[END PROCESSES]" >> ~/Desktop/MSA_LOG.txt
echo "[NETWORK CONFIGURATION]" >> ~/Desktop/MSA_LOG.txt
netconfig
echo "[END NETWORK CONFIGURATION]" >> ~/Desktop/MSA_LOG.txt
newfiles
echo "[INTEGRITY CHECKING]" >> ~/Desktop/MSA_LOG.txt
intcheck
echo "[END INTEGRITY CHECKING]" >> ~/Desktop/MSA_LOG.txt
echo "[LAUNCH DAEMONS]" >> ~/Desktop/MSA_LOG.txt
launchd
echo "[END LAUNCH DAEMONS]" >> ~/Desktop/MSA_LOG.txt
echo "[KEXTS]" >> ~/Desktop/MSA_LOG.txt
kernel
echo "[END KEXTS]" >> ~/Desktop/MSA_LOG.txt
echo "[BROWSER EXTENSIONS]" >> ~/Desktop/MSA_LOG.txt
browserext
echo "[END BROWSER EXTENSIONS]" >> ~/Desktop/MSA_LOG.txt
echo "[AVAILABLE SOFTWARE UPDATES]" >> ~/Desktop/MSA_LOG.txt
swupdate
echo "[END AVAILABLE SOFTWARE UPDATES]" >> ~/Desktop/MSA_LOG.txt
end

